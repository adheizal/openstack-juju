# **How to Use**

```
git clone https://gitlab.com/adhelhgi/openstack-juju.git
cd openstack-juju
chmod +x neutron-ext-net-ksv3
chmod +x neutron-tenant-net-ksv3
```

use rc admin `source openrc`

**Create External network**
```
./neutron-ext-net-ksv3 --network-type flat \
    -g 10.71.71.1 -c 10.71.71.0/24 \
    -f 10.71.71.200:10.71.71.254 ext_net
```

**Create Internal Network**
```
./neutron-tenant-net-ksv3 -p admin -r provider-router \
    -N 1.1.1.1 internal 10.10.10.0/24
```
